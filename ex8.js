function swap(arr,first,second) {
    var temp = arr[first];
    arr[first]= arr[second];
    arr[second]=temp;
    // debugger
    // [arr2[first],arr2[second]] = [arr2[first],arr2[second]];
}

function checkarray() {
    $(".input-array").click(function() {
        let str1 = $("#arr1").val();
        let str2 = $("#arr2").val();
        let arr1 = str1.split('');
        let arr2 = str2.split('');
        var check =0;
        var ascendingNumber1 = arr1.sort((a, b) => a - b); // sort ascending array
        var ascendingNumber2 = arr2.sort((a,b) => a-b);     //sort ascending array
        // check array whether array is equal or not
        if(JSON.stringify(ascendingNumber1) === JSON.stringify(ascendingNumber2)) {
            for(var i=0;i<arr1.length-1;i++) {
                if(arr1[i]==arr2[i]) {
                    continue;
                }
                else {
                    check++;
                    for(var j=i+1;j<arr1.length;j++) {
                        if(arr2[j]==arr1[i]) {
                            swap(arr2,i,j)
                        }
                    }
                }
            }
            if(check<=1) {
                $(".output-check").empty().append("2 array is equal");
                // console.log("2 array is equal");
            }
            else {
                $(".output-check").empty().append("2 array is not equal");
                // console.log("2 array is not equal")
            }
        
        }
        else {
            $(".output-check").empty().append("2 array is not equal");
            // console.log("2 array is not equal");
        }
    })
}
$(document).ready(function() {
    checkarray();
})
